# bubblewrap

An Ansible role to install [bubblewrap](https://github.com/containers/bubblewrap) using [the Debian package](https://packages.debian.org/bookworm/bubblewrap) and configuring it for Evince, LibreOffice and Firefox usng the [valoq profiles](https://github.com/valoq/bwscripts).

## License

This Ansible role is released under the [GNU General Public License v3.0](LICENSE).

The [source for the bubblewrap profiles](https://github.com/valoq/bwscripts/tree/master/profiles) and the [exportFilter.c](https://github.com/valoq/bwscripts/blob/master/exportFilter.c) script used in this Ansible role are released under the [GNU Lesser General Public License v2.1](https://github.com/valoq/bwscripts/blob/master/LICENSE) and were written by [valoq](https://github.com/valoq).

The [bubblewrap image](https://git.coop/uploads/-/system/project/avatar/1178/Bubble-Wrap-Transparent.png) used by the `git.coop` repo hosting this Ansible role is [Creative Commons 4.0 BY-NC](https://www.pngall.com/bubble-wrap-png/download/41220).
